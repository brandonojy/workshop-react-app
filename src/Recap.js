/**
 * Created by brandonong on 15/4/18.
 */
const array = ["id1","id2","id3","id4"]
//console.log(array[2])

const object = {"id1": "value1", "id2":"value2"}
//console.log (object.id1)
//console.log(object["id1"])


const nestedobject =
    {
        "id1": {
            "nesteditem1": "id1nestedvalue1",
            "nesteditem2": "id1nestedvalue2"
        },
        "id2": {
            "nesteditem1": "id2nestedvalue1",
            "nesteditem2": "id2nestedvalue2"
        }
    }

//console.log(nestedobject["id1"]["nesteditem1"])


let newarrayaftermap = []
let numberarray = [1,2,3,4]
numberarray.map(eachitem => {
    let value_to_be_pushed_to_array = eachitem * 2
    newarrayaftermap.push(value_to_be_pushed_to_array)
})
//console.log(newarrayaftermap)
//console.log(array)



const getkeysinfirsttier = Object.keys(nestedobject)
//console.log(getkeysinfirsttier)


let arrayofnestedvalue1 = []
getkeysinfirsttier.map(eachKeyinNestedObject => { // getkeysinfirsttier = ["id1","id2"]
    let value_to_be_pushed_to_array = nestedobject[eachKeyinNestedObject]["nesteditem1"]
    arrayofnestedvalue1.push(value_to_be_pushed_to_array)
})
getkeysinfirsttier.map(function(eachKeyinNestedObject) { // getkeysinfirsttier = ["id1","id2"]
    let value_to_be_pushed_to_array = nestedobject[eachKeyinNestedObject]["nesteditem1"]
    arrayofnestedvalue1.push(value_to_be_pushed_to_array)
})
console.log(arrayofnestedvalue1)

