/**
 * Created by brandonong on 15/4/18.
 */
import React, {Component} from 'react';
import {Elements, injectStripe, CardElement} from 'react-stripe-elements';
import './App.css'
import axios from 'axios'

const handleBlur = () => {
    console.log('[blur]');
};
const handleChange = change => {
    console.log('[change]', change);
};
const handleClick = () => {
    console.log('[click]');
};
const handleFocus = () => {
    console.log('[focus]');
};
const handleReady = () => {
    console.log('[ready]');
};

const createOptions = (fontSize) => {
    return {
        style: {
            base: {
                fontSize,
                color: '#424770',
                letterSpacing: '0.025em',
                fontFamily: 'Source Code Pro, Menlo, monospace',
                '::placeholder': {
                    color: '#aab7c4',
                },
            },
            invalid: {
                color: '#9e2146',
            },
        },
    };
};

class _CardForm extends React.Component {
    handleSubmit = ev => {
        ev.preventDefault();
        var self = this
        this.props.stripe.createToken().then(
            payload => {
                axios.post('https://us-central1-workshopairbnb.cloudfunctions.net/workshoppayment', {
                    stripeToken: payload.token.id,
                    price: parseFloat(self.props.price).toFixed(0),
                })
                    .then(function (response) {
                        console.log(response)
                    })
                    .catch(function (err) {
                        console.log(err)
                    })
            })
    };
    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label style={{width: '100%'}}>
                    Card details
                    <CardElement
                        onBlur={handleBlur}
                        onChange={handleChange}
                        onFocus={handleFocus}
                        onReady={handleReady}
                        {...createOptions(this.props.fontSize)}
                    />
                </label>
                <button>Pay</button>
            </form>
        );
    }
}

const CardForm = injectStripe(_CardForm);

export default CardForm



