import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {StripeProvider} from 'react-stripe-elements';
const testKey = "pk_test_0r53koSp5TSHTtg5DdNHQYo9"

ReactDOM.render(
    <StripeProvider apiKey={testKey}>
        <App />
    </StripeProvider>,
    document.getElementById('root')
);
registerServiceWorker();
