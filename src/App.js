import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import * as firebase from 'firebase'
import * as airbnbjson from './workshopairbnb-export.json'
import {Elements, injectStripe, CardElement} from 'react-stripe-elements';
import {
    BrowserRouter as Router,
    Route,
    Redirect,
    Link,
    Switch
} from 'react-router-dom'
import Cardform from './StripePayment'

var config = {
    apiKey: "AIzaSyAi9xgS_1KGYqFdKA8jzCVay_DDc6AkSmI",
    authDomain: "workshopairbnb.firebaseapp.com",
    databaseURL: "https://workshopairbnb.firebaseio.com",
    projectId: "workshopairbnb",
    storageBucket: "workshopairbnb.appspot.com",
    messagingSenderId: "614275593467"
};
firebase.initializeApp(config);

class App extends React.Component {
    render() {
        return (
            <Router>
                <div>
                    <Route exact path="/" component={ProductApp}/>
                    <Route path="/homes/:homeid" component={EachHomeApp}/>
                </div>
            </Router>
        )
    }
}

class ProductApp extends React.Component {
    render() {
        return (
            <div className="container">
                <h2 style={{paddingLeft: '10px', marginBottom: '10px'}}>Homes around the world</h2>
                <DisplayHomes/>
            </div>
        );
    }
}

class EachHomeApp extends React.Component {

    constructor(props) {
        super(props)
    }

    componentDidMount() {
        firebase.database().ref(`/homes/${this.props.match.params.homeid}`).once('value').then(snapshot => {
            const airbnbjson = snapshot.val()
            console.log(airbnbjson)
        })
    }

    render() {
        console.log(this.props.match.params.homeid)
        return (
            <div>
                <div id="hero-image"/>
                <div className="container" style={{marginTop: '30px'}}>
                    <small><strong style={{color: '#222222'}}>ENTIRE HOUSE</strong></small>
                    <h1>The Joshua Tree House</h1>
                    <h6>Joshua Tree</h6>
                    <div className="d-flex">
                        <div className="facilities">
                            <span><i className="fas fa-users"/> 6 guests</span>
                        </div>
                        <div className="facilities">
                            <span><i className="fas fa-bed"/> 2 bedrooms</span>
                        </div>
                        <div className="facilities">
                            <span><i className="fas fa-bed"/> 2 beds</span>
                        </div>
                        <div className="facilities">
                            <span><i className="fas fa-bath"/> 2 baths</span>
                        </div>
                    </div>
                    <div className="trenddisplay d-flex justify-content-between align-items-center">
                        <div>
                            <h6 style={{fontWeight: '700'}}>This home is on people's mind</h6>
                            <h6>It's been viewed 500+ times in the past week</h6>
                        </div>
                        <div>
                            <img
                                src="https://a0.muscache.com/airbnb/static/page3/icon-uc-light-bulb-b34f4ddc543809b3144949c9e8cfcc8d.gif"/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class DisplayHomes extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            homedata: []
        }
    }

    componentDidMount() {
        firebase.database().ref('/').once('value').then(snapshot => {
            const airbnbjson = snapshot.val()
            let homeid = Object.keys(airbnbjson.homes)
            let homehtml = homeid.map(eachid =>
                <Link className="equal-4" to={`/homes/${eachid}`}>
                    <img className="img-fit-container"
                         src="https://a0.muscache.com/im/pictures/90157089/180b2f6e_original.jpg?aki_policy=large"/>
                    <h5 style={{
                        margin: '5px 0',
                        textTransform: 'uppercase',
                        textOverflow: "ellipsis",
                        overflow: "hidden",
                        whiteSpace: 'nowrap',
                    }}>
                        {airbnbjson["homes"][`${eachid}`]["typeofhouse"]}
                        <span> - {airbnbjson["homes"][`${eachid}`]["city"]}</span>
                    </h5>
                    <h3 style={{
                        margin: '0',
                        textOverflow: "ellipsis",
                        overflow: "hidden",
                        whiteSpace: 'nowrap',
                    }}>{airbnbjson["homes"][`${eachid}`]["name"]}</h3>
                    <div>${airbnbjson["homes"][`${eachid}`]["rates"]} SGD per night</div>
                    <div className="d-flex" style={{alignItems: 'center'}}>
                        <StarRatings starrating={airbnbjson["homes"][`${eachid}`]["starcount"]}/>
                        <small>{airbnbjson["homes"][`${eachid}`]["reviews"]}{airbnbjson["homes"][`${eachid}`]["superhost"] ? " - Superhost" : undefined}</small>
                    </div>
                </Link>
            )
            this.setState({homedata: homehtml})
        });

    }

    render() {
        const homedata = this.state.homedata
        return (
            <div className="d-flex">
                {homedata}
            </div>
        )
    }
}

class StarRatings extends React.Component {

    showStars(starrating) {
        let stars = []
        for (let i = 0; i < starrating; i++) {
            stars.push(<i className="fas fa-star" style={{color: 'darkgreen'}}/>)
        }
        return (stars)
    }

    render() {
        const starrating = this.props.starrating
        return (
            <span style={{fontSize: '8px'}}>
                {this.showStars(starrating)}
            </span>
        )
    }
}

export default App;
